let songs = [
		'Screamtracker 3/Purple Motion/unreal ][.s3m',
		'Screamtracker 3/Purple Motion/zak-zaka-zak-zak.s3m',
		'Soundtracker/Nightlight/fletch.mod',
		'Screamtracker 3/Skaven/2nd reality.s3m',
		'Screamtracker 3/Karsten Koch/aryx.s3m',
		'Fasttracker 2/Falcon (PL)/breakout of bedroom.xm',
		'Fasttracker 2/Cactus/here\'s andy.xm',
		'Screamtracker 3/Nightbeat/children - classic version.s3m',
		'Screamtracker 3/Necros/click.s3m',
		'Screamtracker 3/Nemo/crystal dreams.s3m',
		'Fasttracker 2/Quazar/funky stars.xm',
		'Impulsetracker/Katie Cadet/mission impossible techno.it',
		'Fasttracker 2/Laxity/edge of the world.xm',
		'Screamtracker 3/Sasa Milovic/mission impossible rmx.s3m',
		'Screamtracker 3/Necros/point of departure.s3m',
		'Fasttracker 2/Awesome/a day without her.xm',
		'Impulsetracker/Oracle Soul//raven.it',
		'Screamtracker 3/Armadon/raveyard.s3m',
		'Screamtracker 3/Purple Motion/ssi-intro.s3m',
		'Fasttracker 2/Radix/superglam.xm',
		'Fasttracker 2/Elwood/sweet dreams.xm',
		'HivelyTracker/AceMan/lanterns.hvl',
	];

function toHTML(txt)
{
	return txt.replaceAll("\n", "<br/>").replaceAll(" ", "&nbsp;");
}

class XMPDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "webXMP";}
	getDisplaySubtitle() 	{ return "lets play some music..";}
	getDisplayLine1() { return this.getSongInfo().title;}
	getDisplayLine2() { return this.getSongInfo().player; }
	getDisplayLine3() { return ""; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new XMPBackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new XMPDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x6565e6], 0, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}