/**
 * Helper used for the passing of song meta information to the JavaScript side via EMSCRIPTEN.
 /*/
#ifndef METAINFOHELPER_H
#define METAINFOHELPER_H

#include <wchar.h>
#include <string>
#include <vector>

namespace emsutil {
	class MetaInfoHelper {
	public:
		MetaInfoHelper(int n);
		~MetaInfoHelper();

		/**
		* Array used to pass info to JavsScript side.
		*/
		const char** getMeta();

		/**
		* Clear all the meta information.
		*/
		void clear();

		/**
		* Setters for available meta info attributes.
		*/
		void setWText(unsigned char i, const wchar_t *t, const char* dflt);
		void setText(unsigned char i, const char *t, const char* dflt);
		
	private:
		std::string convert(const wchar_t *src);
	private:
		std::vector<std::string> infoStrings;
		const char** infoCharPtrs;
	};
}
#endif