#include "MetaInfoHelper2.h"


#define __STDC_WANT_LIB_EXT1__ 1 /* mbstowcs */
#include <stdlib.h>     /* malloc, free, rand */
#include <locale.h>

#include <stdio.h>
#include <string.h>

#define TEXT_MAX	16384
static char _tmpBuffer[TEXT_MAX];

emsutil::MetaInfoHelper::MetaInfoHelper(int n) {
	infoStrings.resize(n);
	
	infoCharPtrs = (const char**)malloc(sizeof(char*) * n );
}

emsutil::MetaInfoHelper::~MetaInfoHelper() {
	free(infoCharPtrs);
}


const char** emsutil::MetaInfoHelper::getMeta() 
{
	for (int i = 0; i < infoStrings.size(); i++) {
		infoCharPtrs[i] = infoStrings[i].c_str();
	}
	return infoCharPtrs;
}

void emsutil::MetaInfoHelper::clear() 
{
	for (int i = 0; i < infoStrings.size(); i++) {
		infoStrings[i] = std::string("");
	}
}

std::string emsutil::MetaInfoHelper::convert(const wchar_t *src) 
{
	if (src) 
	{
		setlocale(LC_ALL, "");
		snprintf(_tmpBuffer, TEXT_MAX, "%ls", src);
		return std::string(_tmpBuffer);
	}
	else
	{
		return std::string("");
	}
}

void emsutil::MetaInfoHelper::setWText(unsigned char i, const wchar_t *t,const char* dflt) {
	if (i >= infoStrings.size()) 
	{
		fprintf(stderr, "error: MetaInfoHelper out of bounds access\n");
		exit(-1);
	}
	
	infoStrings[i] = t ? convert(t) : std::string(dflt);
}

void emsutil::MetaInfoHelper::setText(unsigned char i, const char *t, const char* dflt) 
{
	if (i >= infoStrings.size()) 
	{
		fprintf(stderr, "error: MetaInfoHelper out of bounds access\n");
		exit(-1);
	}
	infoStrings[i] = t ? std::string(t) : std::string(dflt);
}

