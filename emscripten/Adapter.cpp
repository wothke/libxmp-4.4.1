/*
* This is the interface exposed by Emscripten to the JavaScript world..
*
* Copyright (C) 2018-2023 Juergen Wothke
*
*
* note: I temporarily added HivelyPlayer impl from zxtune (it is easier to activate the
* code here than to update my old zxtune version.. Also the dead code in xmp suggests
* that hvl support is envisoned and that my temporary add-on might become obsolete soon ;-)
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <emscripten.h>

#include <xmp.h>
#include "common.h"

#include <hvl_replay.h>

#include "MetaInfoHelper2.h"
using emsutil::MetaInfoHelper;

#define CHANNELS 2
#define BYTES_PER_SAMPLE 2


namespace xmp {
	class Adapter {
	public:
		Adapter() : _isHvlMode(false), _sampleRate(0), _sampleBuffer(NULL), _sampleBufferSize(0),
					_ht(NULL), _htTotalFrames(0), _info(5)
		{
		}

		int loadFile(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: sampleRate, audioBufSize, scopesEnabled support not implemented

			if (sampleRate < XMP_MIN_SRATE) sampleRate = XMP_MIN_SRATE;	// use resampling outside of this range
			if (sampleRate > XMP_MAX_SRATE) sampleRate = XMP_MAX_SRATE;

			_sampleRate = sampleRate;

			_isHvlMode = !strncmp("HVL", (const char *)inBuffer, 3);
			if (_isHvlMode)
			{
				if (_sampleBuffer == 0)	hvl_InitReplayer();
				
				_hvl_comments = std::string("");

				_ht = hvl_ParseTune( (unsigned char *)inBuffer, inBufSize, _sampleRate, 0 );
				return (_ht == 0);
			}
			else
			{
				_ctx = xmp_create_context();
				return xmp_load_module_from_memory(_ctx, (void*)inBuffer, inBufSize);
			}
		}

		void addToComments(const char* txt)
		{
			_hvl_comments += std::string(txt) + std::string("\n");
		}
		
		int setSubsong(int trackId)
		{
			if (_isHvlMode)
			{
				if ((trackId < 0) || (trackId >= _ht->ht_SubsongNr))
				{
					trackId = 0;
				}
				hvlSetupTrack(trackId);
			}
			else
			{
				if (xmp_start_player(_ctx, _sampleRate, 0)) return 1;	// error
			}

			updateTrackInfo();

			return 0;
		}

		void teardown()
		{
			if (_ht)
			{
				hvl_FreeTune( _ht );
				_ht = NULL;
			}

			if (_ctx)
			{
	// 			xmp_stop_module(_ctx) 			fixme: use instead and reuse context?

	//			xmp_end_player(_ctx);
	//			xmp_release_module(_ctx);        // includes xmp_end_player
				xmp_free_context(_ctx);         // includes xmp_release_module
				_ctx = NULL;
			}
		}

		int getSampleRate()
		{
			return _sampleRate;
		}


		int getSampleBufferLength()
		{
			if (_isHvlMode)
			{
				return _sampleBufferSize;	// in bytes
			}
			else
			{
				return _fi.buffer_size >> 2;
			}
		}

		char* getSampleBuffer()
		{
			if (_isHvlMode)
			{
				return (char*)_sampleBuffer;
			}
			else
			{
				return (char*)_fi.buffer;
			}
		}

		int getCurrentPosition()
		{
			if (_isHvlMode)
			{
				return _ht->ht_PlayingTime * 1000 / 50;
			}
			else
			{
				return _fi.time;
			}
		}

		void seekPosition(int ms) {
			if (_isHvlMode)
			{
				uint32 current = _ht->ht_PlayingTime;
				if (ms < current)
				{
					hvl_InitSubsong(_ht, 0);
					current = 0;
				}
				for (; current < ms; ++current) {
					hvl_NextFrame(_ht);
				}
			}
			else
			{
				xmp_seek_time(_ctx, ms);
			}
		}

		int getMaxPosition()
		{
			if (_isHvlMode)
			{
				return _htTotalFrames * 1000 / 50;
			}
			else
			{
				return _fi.total_time;
			}
		}

		int genSamples()
		{
			int status = playFrame();
			if (status) return status;						// means "end song"

			return isSongEnd();
		}

		const char** getMeta()
		{
			return _info.getMeta();
		}
	private:
		int end()
		{
			if (_isHvlMode)
			{
				hvl_FreeTune( _ht );
				_ht = NULL;
			}
			else
			{
				xmp_end_player(_ctx);
				xmp_release_module(_ctx);        /* unload module */
				xmp_free_context(_ctx);          /* destroy the player context */
			}
			return 0;
		}

		void hvlSetupTrack(int trackId)
		{
			hvl_InitSubsong(_ht, trackId);

			_htTotalFrames = 0;

			while (!_ht->ht_SongEndReached) {
				hvl_NextFrame(_ht);
				_htTotalFrames++;
			}

			hvl_InitSubsong(_ht, trackId);	// restart
		}

		int playFrame()
		{
			if (_isHvlMode) {
				uint32 samples = hvl_getSamplesPerFrame(_ht);
				if (!_sampleBuffer || (samples > _sampleBufferSize))
				{
					if (_sampleBuffer) free(_sampleBuffer);

					_sampleBuffer = (int16*)malloc(samples*CHANNELS*BYTES_PER_SAMPLE);
					_sampleBufferSize = samples;
				}
				if (!_ht->ht_SongEndReached)
				{
					hvl_DecodeFrame(_ht, (int8*)_sampleBuffer, (int8*)_sampleBuffer+BYTES_PER_SAMPLE, CHANNELS*BYTES_PER_SAMPLE);	// interleave left/right channel
				}
				return _ht->ht_SongEndReached;	// 0=OK
			}
			else
			{
				return  xmp_play_frame(_ctx);
			}
		}

		int isSongEnd()
		{
			if (_isHvlMode)
			{
				return _ht->ht_SongEndReached;	// 1= end song
			}
			else
			{
				xmp_get_frame_info(_ctx, &_fi);
				return _fi.loop_count;
			}
		}

		void updateTrackInfo()
		{
			if (_isHvlMode)
			{
				_info.setText(0, _ht->ht_Name, "");
				_info.setText(1, "", "");
				_info.setText(2, std::to_string(_ht->ht_SubsongNr).c_str(), "");
				_info.setText(3, _hvl_comments.c_str(), "");
				_info.setText(4, "", "");
			}
			else
			{
				// issue: when initially starting FuchsTracker/lowtheme.fuchs, then the type field reports garbage
				// but after some reloads it changes to "Fuchs Tracker" .. WTF?

				xmp_get_module_info(_ctx, &_mi);

				_info.setText(0, _mi.mod->name, "");
				_info.setText(1, _mi.mod->type, "");
				_info.setText(2, "0", "");
				_info.setText(3, _mi.comment, "");
								
				std::string instNames = std::string("");
				
				for (int i = 0; i < _mi.mod->ins; i++) {
					instNames += std::string(_mi.mod->xxi[i].name) + std::string("\n");
				}
				_info.setText(4, instNames.c_str(), "");				
			}
		}		
	private:
		int16 *_sampleBuffer;
		int _sampleBufferSize;
		int _sampleRate;
		int _isHvlMode;

		// regular XMP stuff:
		xmp_context _ctx;				// xmp_context is just a char*
		struct xmp_frame_info _fi;
		struct xmp_module_info _mi;

		// temporary HivelyTracker add-on:
		struct hvl_tune *_ht;
		int _htTotalFrames;
		std::string _hvl_comments;
		
		MetaInfoHelper _info;
	};
};

xmp::Adapter _adapter;


extern "C" {
void add_to_comments(const char* txt) { _adapter.addToComments(txt); }
};

// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard functions
EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return _adapter.setSubsong(track); }
EMBIND(const char**, emu_get_track_info())			{ return _adapter.getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))			{ _adapter.seekPosition(ms);  }
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }

